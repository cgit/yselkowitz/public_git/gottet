Name:           gottet
Version:        1.2.0
Release:        1%{?dist}
Summary:        Falling blocks game
License:        GPLv3+
URL:            http://gottcode.org/%{name}/
Source0:        http://gottcode.org/%{name}/%{name}-%{version}-src.tar.bz2

BuildRequires:  desktop-file-utils
BuildRequires:  libappstream-glib
BuildRequires:  qt5-qttools-devel

Requires:       hicolor-icon-theme

%description
Gottet is a clean, simple falling blocks game. Use the arrow keys to rotate
and position each piece as it falls. Fill a whole row to clear it. Unlimited
levels, with progressively faster speeds.

%prep
%setup -q

%build
%{qmake_qt5} PREFIX=%{_prefix}
make %{?_smp_mflags}

%install
make install INSTALL_ROOT=%{buildroot}

%find_lang %{name} --with-qt --without-mo

%check
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}.desktop || :
appstream-util validate-relax --nonet %{buildroot}%{_datadir}/metainfo/%{name}.appdata.xml || :

%files -f %{name}.lang
%doc ChangeLog CREDITS README
%license COPYING
%{_bindir}/%{name}
%{_datadir}/metainfo/%{name}.appdata.xml
%{_datadir}/applications/%{name}.desktop
%dir %{_datadir}/%{name}/
%dir %{_datadir}/%{name}/translations
%{_datadir}/icons/hicolor/*/apps/%{name}.*
%{_mandir}/man6/%{name}.6.*


%changelog
* Mon Jun 14 2021 Yaakov Selkowitz <yselkowi@redhat.com> - 1.2.0-1
- new version

* Fri Jul 31 2020 Yaakov Selkowitz <yselkowi@redhat.com> - 1.1.9-1
- Initial package
